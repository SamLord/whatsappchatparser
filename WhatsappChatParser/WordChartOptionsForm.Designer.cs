﻿namespace WhatsappChatParser
{
    partial class WordChartOptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ignoreCaseCheckBox = new System.Windows.Forms.CheckBox();
            this.removePunctuationCheckBox = new System.Windows.Forms.CheckBox();
            this.ignoreSystemMessagesCheckBox = new System.Windows.Forms.CheckBox();
            this.ignoreMediaOmmittedCheckBox = new System.Windows.Forms.CheckBox();
            this.ignoreFromWordListCheckBox = new System.Windows.Forms.CheckBox();
            this.selectWordListButton = new System.Windows.Forms.Button();
            this.createChartButton = new System.Windows.Forms.Button();
            this.openFileMenu = new System.Windows.Forms.OpenFileDialog();
            this.numberOfWordsLabel = new System.Windows.Forms.Label();
            this.numberOfWordsNumericUpDown = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfWordsNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // ignoreCaseCheckBox
            // 
            this.ignoreCaseCheckBox.AutoSize = true;
            this.ignoreCaseCheckBox.Checked = true;
            this.ignoreCaseCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ignoreCaseCheckBox.Location = new System.Drawing.Point(10, 45);
            this.ignoreCaseCheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.ignoreCaseCheckBox.Name = "ignoreCaseCheckBox";
            this.ignoreCaseCheckBox.Size = new System.Drawing.Size(83, 17);
            this.ignoreCaseCheckBox.TabIndex = 0;
            this.ignoreCaseCheckBox.Text = "Ignore Case";
            this.ignoreCaseCheckBox.UseVisualStyleBackColor = true;
            // 
            // removePunctuationCheckBox
            // 
            this.removePunctuationCheckBox.AutoSize = true;
            this.removePunctuationCheckBox.Checked = true;
            this.removePunctuationCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.removePunctuationCheckBox.Location = new System.Drawing.Point(10, 67);
            this.removePunctuationCheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.removePunctuationCheckBox.Name = "removePunctuationCheckBox";
            this.removePunctuationCheckBox.Size = new System.Drawing.Size(126, 17);
            this.removePunctuationCheckBox.TabIndex = 1;
            this.removePunctuationCheckBox.Text = "Remove Punctuation";
            this.removePunctuationCheckBox.UseVisualStyleBackColor = true;
            // 
            // ignoreSystemMessagesCheckBox
            // 
            this.ignoreSystemMessagesCheckBox.AutoSize = true;
            this.ignoreSystemMessagesCheckBox.Checked = true;
            this.ignoreSystemMessagesCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ignoreSystemMessagesCheckBox.Location = new System.Drawing.Point(9, 89);
            this.ignoreSystemMessagesCheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.ignoreSystemMessagesCheckBox.Name = "ignoreSystemMessagesCheckBox";
            this.ignoreSystemMessagesCheckBox.Size = new System.Drawing.Size(144, 17);
            this.ignoreSystemMessagesCheckBox.TabIndex = 2;
            this.ignoreSystemMessagesCheckBox.Text = "Ignore System Messages";
            this.ignoreSystemMessagesCheckBox.UseVisualStyleBackColor = true;
            // 
            // ignoreMediaOmmittedCheckBox
            // 
            this.ignoreMediaOmmittedCheckBox.AutoSize = true;
            this.ignoreMediaOmmittedCheckBox.Checked = true;
            this.ignoreMediaOmmittedCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ignoreMediaOmmittedCheckBox.Location = new System.Drawing.Point(9, 110);
            this.ignoreMediaOmmittedCheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.ignoreMediaOmmittedCheckBox.Name = "ignoreMediaOmmittedCheckBox";
            this.ignoreMediaOmmittedCheckBox.Size = new System.Drawing.Size(196, 17);
            this.ignoreMediaOmmittedCheckBox.TabIndex = 3;
            this.ignoreMediaOmmittedCheckBox.Text = "Ignore \"Media Ommitted\" Messages";
            this.ignoreMediaOmmittedCheckBox.UseVisualStyleBackColor = true;
            // 
            // ignoreFromWordListCheckBox
            // 
            this.ignoreFromWordListCheckBox.AutoSize = true;
            this.ignoreFromWordListCheckBox.Location = new System.Drawing.Point(9, 134);
            this.ignoreFromWordListCheckBox.Margin = new System.Windows.Forms.Padding(2);
            this.ignoreFromWordListCheckBox.Name = "ignoreFromWordListCheckBox";
            this.ignoreFromWordListCheckBox.Size = new System.Drawing.Size(130, 17);
            this.ignoreFromWordListCheckBox.TabIndex = 4;
            this.ignoreFromWordListCheckBox.Text = "Ignore From Word List";
            this.ignoreFromWordListCheckBox.UseVisualStyleBackColor = true;
            this.ignoreFromWordListCheckBox.CheckStateChanged += new System.EventHandler(this.ignoreFromWordListCheckBox_CheckStateChanged);
            // 
            // selectWordListButton
            // 
            this.selectWordListButton.Enabled = false;
            this.selectWordListButton.Location = new System.Drawing.Point(9, 156);
            this.selectWordListButton.Margin = new System.Windows.Forms.Padding(2);
            this.selectWordListButton.Name = "selectWordListButton";
            this.selectWordListButton.Size = new System.Drawing.Size(105, 19);
            this.selectWordListButton.TabIndex = 5;
            this.selectWordListButton.Text = "Select Word List";
            this.selectWordListButton.UseVisualStyleBackColor = true;
            this.selectWordListButton.Click += new System.EventHandler(this.selectWordListButton_Click);
            // 
            // createChartButton
            // 
            this.createChartButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.createChartButton.Location = new System.Drawing.Point(146, 240);
            this.createChartButton.Margin = new System.Windows.Forms.Padding(2);
            this.createChartButton.Name = "createChartButton";
            this.createChartButton.Size = new System.Drawing.Size(78, 19);
            this.createChartButton.TabIndex = 6;
            this.createChartButton.Text = "Create Chart";
            this.createChartButton.UseVisualStyleBackColor = true;
            this.createChartButton.Click += new System.EventHandler(this.createChartButton_Click);
            // 
            // openFileMenu
            // 
            this.openFileMenu.FileName = "openFileDialog1";
            // 
            // numberOfWordsLabel
            // 
            this.numberOfWordsLabel.AutoSize = true;
            this.numberOfWordsLabel.Location = new System.Drawing.Point(7, 192);
            this.numberOfWordsLabel.Name = "numberOfWordsLabel";
            this.numberOfWordsLabel.Size = new System.Drawing.Size(90, 13);
            this.numberOfWordsLabel.TabIndex = 8;
            this.numberOfWordsLabel.Text = "Number of Words";
            // 
            // numberOfWordsNumericUpDown
            // 
            this.numberOfWordsNumericUpDown.Location = new System.Drawing.Point(101, 190);
            this.numberOfWordsNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numberOfWordsNumericUpDown.Name = "numberOfWordsNumericUpDown";
            this.numberOfWordsNumericUpDown.Size = new System.Drawing.Size(68, 20);
            this.numberOfWordsNumericUpDown.TabIndex = 9;
            this.numberOfWordsNumericUpDown.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // WordChartOptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(233, 268);
            this.Controls.Add(this.numberOfWordsNumericUpDown);
            this.Controls.Add(this.numberOfWordsLabel);
            this.Controls.Add(this.createChartButton);
            this.Controls.Add(this.selectWordListButton);
            this.Controls.Add(this.ignoreFromWordListCheckBox);
            this.Controls.Add(this.ignoreMediaOmmittedCheckBox);
            this.Controls.Add(this.ignoreSystemMessagesCheckBox);
            this.Controls.Add(this.removePunctuationCheckBox);
            this.Controls.Add(this.ignoreCaseCheckBox);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "WordChartOptionsForm";
            this.Text = "WordChartOptionsForm";
            ((System.ComponentModel.ISupportInitialize)(this.numberOfWordsNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox ignoreCaseCheckBox;
        private System.Windows.Forms.CheckBox removePunctuationCheckBox;
        private System.Windows.Forms.CheckBox ignoreSystemMessagesCheckBox;
        private System.Windows.Forms.CheckBox ignoreMediaOmmittedCheckBox;
        private System.Windows.Forms.CheckBox ignoreFromWordListCheckBox;
        private System.Windows.Forms.Button selectWordListButton;
        private System.Windows.Forms.Button createChartButton;
        private System.Windows.Forms.OpenFileDialog openFileMenu;
        private System.Windows.Forms.Label numberOfWordsLabel;
        private System.Windows.Forms.NumericUpDown numberOfWordsNumericUpDown;
    }
}