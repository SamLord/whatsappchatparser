﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WhatsappChatParser
{
    public partial class WordChartOptionsForm : Form
    {
        WhatsappChat currentChat;
        string[] ignoredWords = null;

        public WordChartOptionsForm(WhatsappChat currentChat)
        {
            InitializeComponent();
            this.currentChat = currentChat;
        }

        private void ignoreFromWordListCheckBox_CheckStateChanged(object sender, EventArgs e)
        {
            selectWordListButton.Enabled = ignoreFromWordListCheckBox.Checked;
        }

        private void createChartButton_Click(object sender, EventArgs e)
        {
            ChartView chartView = new ChartView();
            chartView.Show();
            Dictionary<string, int> wordCount = currentChat.GetWordDistribution(ignoreCaseCheckBox.Checked, removePunctuationCheckBox.Checked, ignoreSystemMessagesCheckBox.Checked, ignoreMediaOmmittedCheckBox.Checked, ignoredWords);
                
            //sort and limit to top 10
            wordCount = wordCount.OrderByDescending(pair => pair.Value).Take((int)numberOfWordsNumericUpDown.Value).ToDictionary(pair => pair.Key, pair => pair.Value);

            chartView.ReplaceData<string, int>(wordCount);
        }

        private void selectWordListButton_Click(object sender, EventArgs e)
        {
            openFileMenu.ShowDialog();

            if (File.Exists(openFileMenu.FileName))
            {
                ignoredWords = File.ReadAllLines(openFileMenu.FileName);
                MessageBox.Show("Loaded " + ignoredWords.Length + " words to ignore");
            }
            else
            {
                MessageBox.Show("File doesn't exist");
                selectWordListButton.Enabled = false;
                ignoreFromWordListCheckBox.Checked = false;
            }
        }

        private void textBox1_Validating(object sender, CancelEventArgs e)
        {

        }
    }
}
