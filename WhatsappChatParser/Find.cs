﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WhatsappChatParser
{
    public partial class Find : Form
    {
        WhatsappChat chat;

        public Find(WhatsappChat chat)
        {
            InitializeComponent();
            this.chat = chat;
        }

        private void findButton_Click(object sender, EventArgs e)
        {
            List<Message> matches = new List<Message>();
            string query = findTextBox.Text;
            if (query != "")
            {

                foreach (Message msg in chat.AllMessages)
                {
                    bool nameFound = msg.Sender.ToString().Contains(query);
                    bool dateTimeFound = msg.SentDateTime.ToString().Contains(query);
                    bool bodyFound = msg.Body.Contains(query);

                    if (nameFound || bodyFound || dateTimeFound)
                    {
                        matches.Add(msg);
                    }
                }
                FindResult resultUI = new FindResult(matches);
                resultUI.Show();
            }
        }
    }
}
